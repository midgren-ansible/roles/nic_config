import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_lan0(host):
    # This tests that there is a lan0 interface and that is has IP 10.250.12.1:
    assert host.run_expect([0], "ip addr show dev lan0 | grep 10.250.12.1/24")
    # Check that a static route has been added according to config:
    assert host.run_expect([0], "ip route | grep '10.250.13.0/24 via 10.250.12.1 dev lan0'")
    assert host.run_expect([0], "ip route | grep '10.250.14.0/24 via 10.250.12.1 dev lan0 proto static metric 1200'")

def test_lan1(host):
    # This tests that there is a lan1 interface and that is has got an IP address
    assert host.run_expect([0], "ip addr show dev lan1 | egrep 'inet[[:space:]]'")

def test_lan2(host):
    # There should be a lan2 if with corresponding MAC
    assert host.run_expect([0], "ip addr show dev lan2 | grep 'link/ether 08:00:27:de:40:a6'")
    # It should not have an IPv4 address though
    assert host.run_expect([1], "ip addr show dev lan2 | egrep 'inet[[:space:]]'")

def test_lan3(host):
    # There should be a lan3 if with corresponding MAC
    assert host.run_expect([0], "ip addr show dev lan3 | grep 'link/ether 08:00:27:de:40:a7'")
    # There should be a bridge if named lanbr with IP 10.250.13.4/24
    assert host.run_expect([0], "ip addr show dev lanbr | grep 10.250.13.4/24")
