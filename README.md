# nic_config role

Ansible role to configure network interfaces and set their names.

The role depends on using netplan for network configuration and can
thus only be used on OSs that supports that. Currently only tested
with Ubuntu 18.04.

## Requirements

- ansible >= 2.7
- molecule >= 2.19
- netplan network configuration

## Role Variables

* `nic_config_interfaces` (optional)

    Dictionary of available network interfaces on the host. Key is the
    interface name and value is another dictionary with additional
    information:

      - macaddress: 01:23:45:67:89:ab  (required)
      - wakeonlan: true / false        (optional)

    The MAC address may be necessary to identify the nic. The network
    interface will be renamed so that the interface with the given MAC
    address gets the specified name.

    The idea behind renaming network interfaces is that network
    interfaces connected to a certain network can have the same name
    on all hosts. This makes it easier to identify an interface and
    makes it possible to define sub-interfaces in an interoperable
    manner since they can have the same parent (name) on all hosts.

    If not provided, this data will be constructed from the
    nic_config_attached dict, where all interfaces must appear with
    their current name. Also by specifying nic_config_interfaces,
    interfaces that are not configured can be specified.

    Example:
    ```yaml
    nic_config_interfaces:
      lan0:
        macaddress: 08:00:27:de:40:a7
        wakeonlan: true
      dmz:
        macaddress: 08:00:27:de:40:af
    ```

* `nic_config_attached`

    This is a dictionary that tells which interfaces should be
    configured on the host itself. At least one interface must be able
    to access the host so that it can be remotely managed on the
    LAN. (Some interfaces are not appropriate to attach, e.g. there is
    probably no need for DMZ network to be attached to the host but
    only to containers that host services available through DMZ.)

    If an IP address should not be set directly to the interface but
    it should rather be used with a bond, vlan or bridge, it must not
    be listed here.

    The dictionary has two key-value-pairs:

    * `ip`

        the IP address or the string 'dhcp'

    * `network`

        (only required with static IP) a dictionary containing
        information about the network (see TODO)

    Example:
    ```yaml
    nic_config_attached:
      lan0:
        ip: 172.22.160.19
        network:
          lan_ip: 172.22.0.0/16
          gateway: 172.22.255.1
          dns_servers: [172.22.192.2,172.22.192.3]
          dns_search_domains: [precise.local, miprecise.com]
          routes: [{destination: 172.17.0.0/16, gateway: 172.22.255.254, metric: 1000}]
      dmz:
        ip: dhcp
    ```

    Please note that _network_ is preferably defined in a central
    location and reused whenever necessary.

* `nic_config_bonds`

    Map of bonds to define (if any). Each map entry is

    - key - the name of the bond
    - value - a second level map with details about the bond:

    - `interfaces` - list of interface names to include in the bond
    - `parameters` - map of parameters to add to the bond
         configuration - in accordance with netplan parameters
         (especially the `mode` parameter may be worth considering)

    Please see details in [netplan documentation](https://netplan.io/reference/#properties-for-device-type-bonds%3A)!

* `nic_config_vlans`

    Map of VLANs to define. Each map entry is

    - key - the id of the vlan,
    - value - a second level map with details about the VLAN.

    The map with vlan details has the following keys:

    - `link` - interface that offers the physical connection to the
      network (referencing entries in `nic_config_interfaces`).
    - `ip` - (Optional) The IP address of host on this vlan
    - `network` - The network this vlan is part of (from `networks`)

    **Note**: The VLAN id must be provided by the network (vlan_id
        parameter)

* `nic_config_bridges`

    Map of bridges to define. Each map entry is

    - key - the id of the bridge,
    - value - a second level map with details about the bridge.

    The map with bridge details has the following keys:

    - `interfaces` - list of interfaces (referencing entries in
      `nic_config_interfaces`) to attach to the bridge. (VLANs may
      also be attached to the bridge in a simlar manner.)
    - `ip` - The IP address of host on this bridge
    - `network` - The network this bridge is part of (from `networks`)

* `nic_config_netplan_file` (optional)

    The file under `/etc/netplan` to save configuration to. This
    defaults to `20-nic-config.yaml` and does normally not require any
    change. If the host is deployed using other tools, e.g. Cloud
    Init, it may use another file that nic_config can later
    overwrite. In those circumstances it's great to be able to specify
    another file name.


* `nic_config_molecule_netplan_file` (optional)

    When testing this role with molecule there may be an issue with
    network configuration, since molecule adds its own interface to
    containers and configures it using netplan (if applicable).

    nic_config will assume that all configuration saved in
    /etc/netplan was put there e.g. by an OS installer and should be
    commented out to avoid it from interfering with the configuration
    the role adds itself. To save molecule's configuration from being
    made inactive, simply add the name of the netplan file to this
    variable.

    Depending on the molecule driver, this file name may be:
    * `50-vagrant.yaml`
    * `10-lxc.yaml`

    In situations where the role is not run by molecule, e.g. in
    production, this variable sholud be left undefined or empty.

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: nic_config
```
