---  # nic_config tasks

- name: Check that this is run on a supported OS and version
  assert:
    that:
      - ansible_distribution == "Ubuntu"
      - ansible_distribution_major_version|int >= 18
    fail_msg: "This role is designed and tested for Ubuntu 18.04 or newer"

- name: Check if we need to set nic_config_interfaces
  set_fact:
    create_nic_config_interfaces: "{{ nic_config_interfaces is not defined }}"

- name: Set nic_config_interfaces if unset
  set_fact:
    nic_config_interfaces: "{{ nic_config_interfaces | default({}) | combine({item.key: ansible_facts[item.key].macaddress}) }}"
  with_dict: "{{ nic_config_attached | default({}) }}"
  when: create_nic_config_interfaces

- name: Check if the managed netplan file exists
  stat:
    path: "/etc/netplan/{{ nic_config_netplan_file }}"
  register: stat_result

- name: Copy netplan stub to host
  copy:
    content: |
      ---
      network:
        version: 2
        renderer: networkd
        ethernets:
    dest: "/etc/netplan/{{ nic_config_netplan_file }}"
    owner: root
    group: root
    mode: 0644
  when: not stat_result.stat.exists

- name: Check network interfaces and maybe reconfigure
  include_tasks: interface.yml
  loop: "{{ nic_config_interfaces | default({}) | dict2items }}"
  loop_control:
    loop_var: interface

- block:

    - name: Add bonds header if needed
      blockinfile:
        path: "/etc/netplan/{{ nic_config_netplan_file }}"
        insertafter: EOF
        block: |2
            bonds:
        marker: "# {mark} bonds header"

    - name: Check bonds
      include_tasks: bond.yml
      loop: "{{ nic_config_bonds | default({}) | dict2items }}"
      loop_control:
        loop_var: bond

  when: nic_config_bonds is defined and nic_config_bonds | length() > 0

- block:

    - name: Add vlans header if needed
      blockinfile:
        path: "/etc/netplan/{{ nic_config_netplan_file }}"
        insertafter: EOF
        block: |2
            vlans:
        marker: "# {mark} vlans header"

    - name: Check vlans
      include_tasks: vlan.yml
      loop: "{{ nic_config_vlans | default({}) | dict2items }}"
      loop_control:
        loop_var: vlan

  when: nic_config_vlans is defined  and nic_config_vlans | length() > 0

- block:

    - name: Add bridges header if needed
      blockinfile:
        path: "/etc/netplan/{{ nic_config_netplan_file }}"
        insertafter: EOF
        block: |2
            bridges:
        marker: "# {mark} bridges header"

    - name: Check bridges
      include_tasks: bridge.yml
      loop: "{{ nic_config_bridges | default({}) | dict2items }}"
      loop_control:
        loop_var: bridge

  when: nic_config_bridges is defined  and nic_config_bridges | length() > 0

- name: Specify pattern of netplan files to keep
  set_fact:
    netplan_pattern: "{{ nic_config_netplan_file }}"
  when: nic_config_molecule_netplan_file | default('') | length() == 0

- name: Specify pattern of netplan files to keep when running molecule
  set_fact:
    netplan_pattern: "({{ nic_config_netplan_file }}|{{ nic_config_molecule_netplan_file }})"
  when: nic_config_molecule_netplan_file | default('') | length() > 0

- name: List netplan files we don't control
  shell: ls /etc/netplan/*.yaml | egrep -v "{{ netplan_pattern }}"
  register: netplans
  changed_when: false
  ignore_errors: true

- name: Put out netplan files that we don't control so that they are not used
  command: "mv {{ item }} {{ item }}.backup"
  loop: "{{ netplans.stdout_lines }}"
  when: not netplans.failed
  notify: netplan apply
